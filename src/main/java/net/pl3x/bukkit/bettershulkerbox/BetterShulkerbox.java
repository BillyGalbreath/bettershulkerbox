package net.pl3x.bukkit.bettershulkerbox;

import net.pl3x.bukkit.bettershulkerbox.command.CmdBetterShulkerbox;
import net.pl3x.bukkit.bettershulkerbox.configuration.Config;
import net.pl3x.bukkit.bettershulkerbox.configuration.Lang;
import net.pl3x.bukkit.bettershulkerbox.listener.BukkitListener;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class BetterShulkerbox extends JavaPlugin {
    @Override
    public void onEnable() {
        Config.reload();
        Lang.reload();

        getServer().getPluginManager().registerEvents(new BukkitListener(), this);

        getCommand("bettershulkerbox").setExecutor(new CmdBetterShulkerbox(this));

        Logger.info(getName() + " v" + getDescription().getVersion() + " enabled!");
    }

    @Override
    public void onDisable() {
        Logger.info(getName() + " disabled.");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&4" + getName() + " is disabled. See console log for more information."));
        return true;
    }

    public static BetterShulkerbox getPlugin() {
        return BetterShulkerbox.getPlugin(BetterShulkerbox.class);
    }
}
