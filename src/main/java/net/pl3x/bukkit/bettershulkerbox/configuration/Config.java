package net.pl3x.bukkit.bettershulkerbox.configuration;

import net.pl3x.bukkit.bettershulkerbox.BetterShulkerbox;
import org.bukkit.configuration.file.FileConfiguration;

public class Config {
    public static boolean COLOR_LOGS = true;
    public static boolean DEBUG_MODE = false;
    public static String LANGUAGE_FILE = "lang-en.yml";
    public static boolean ON_RIGHT_CLICK_AIR = true;
    public static boolean ON_PLACEMENT_CANCELLED = true;

    public static void reload() {
        BetterShulkerbox plugin = BetterShulkerbox.getPlugin();
        plugin.saveDefaultConfig();
        plugin.reloadConfig();
        FileConfiguration config = plugin.getConfig();

        COLOR_LOGS = config.getBoolean("color-logs", true);
        DEBUG_MODE = config.getBoolean("debug-mode", false);
        LANGUAGE_FILE = config.getString("language-file", "lang-en.yml");
        ON_RIGHT_CLICK_AIR = config.getBoolean("on-right-click-air", true);
        ON_PLACEMENT_CANCELLED = config.getBoolean("on-placement-cancelled", true);
    }
}
