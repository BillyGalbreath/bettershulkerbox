package net.pl3x.bukkit.bettershulkerbox.listener;

import net.pl3x.bukkit.bettershulkerbox.BetterShulkerbox;
import net.pl3x.bukkit.bettershulkerbox.configuration.Config;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.ShulkerBox;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BukkitListener implements Listener {
    private final Map<UUID, ItemStack> opened = new HashMap<>();

    @EventHandler(priority = EventPriority.MONITOR)
    public void onOpenShulker(BlockPlaceEvent event) {
        if (!Config.ON_PLACEMENT_CANCELLED) {
            return; // feature disabled
        }

        if (!event.isCancelled()) {
            return; // not cancelled, player can place shulkerbox
        }

        ItemStack shulkerBox = event.getItemInHand();
        if (shulkerBox == null || !isShulkerBox(shulkerBox.getType())) {
            return; // not placing a shulkerbox
        }

        openShulker(event.getPlayer(), (ShulkerBox) ((BlockStateMeta) shulkerBox.getItemMeta()).getBlockState());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onOpenShulker(PlayerInteractEvent event) {
        if (!Config.ON_RIGHT_CLICK_AIR) {
            return; // feature disabled
        }

        if (event.getHand() != EquipmentSlot.HAND) {
            return; // not main hand packet
        }

        if (event.getAction() != Action.RIGHT_CLICK_AIR) {
            return; // not right clicking
        }

        ItemStack shulkerBox = event.getItem();
        if (shulkerBox == null || !isShulkerBox(shulkerBox.getType())) {
            return; // not holding a shulkerbox
        }

        openShulker(event.getPlayer(), (ShulkerBox) ((BlockStateMeta) shulkerBox.getItemMeta()).getBlockState());
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (saveShulkerboxInventory(player, event.getInventory(), false)) {
            event.setCancelled(true);
            player.closeInventory();
            player.updateInventory();
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryDragEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (saveShulkerboxInventory(player, event.getInventory(), false)) {
            event.setCancelled(true);
            player.closeInventory();
            player.updateInventory();
        }
    }

    @EventHandler
    public void onCloseShulkerbox(InventoryCloseEvent event) {
        saveShulkerboxInventory((Player) event.getPlayer(), event.getInventory(), true);
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        opened.remove(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        opened.remove(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onShulkerInception(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        if (!opened.containsKey(player.getUniqueId())) {
            return; // does not have shulkerbox open
        }

        ItemStack itemStack = event.getCurrentItem();
        if (itemStack == null || !isShulkerBox(itemStack.getType())) {
            return; // not clicking on a shulker box
        }

        // dont allow clicking shulker box items (prevents shulker-ception)
        event.setCancelled(true);
        player.closeInventory();
        player.updateInventory();
    }

    private void openShulker(Player player, ShulkerBox shulkerBox) {
        Inventory fakeInv = Bukkit.createInventory(player, 27, "Shulker Box");
        fakeInv.setContents(shulkerBox.getInventory().getContents());

        player.openInventory(fakeInv);

        opened.put(player.getUniqueId(), player.getInventory().getItemInMainHand());

        // check for hotbar swap item exploit attempt
        int initialSlot = player.getInventory().getHeldItemSlot();
        Bukkit.getScheduler().runTaskLater(BetterShulkerbox.getPlugin(), () -> {
            if (initialSlot != player.getInventory().getHeldItemSlot()) {
                player.closeInventory();
                player.updateInventory();
            }
        }, 2);
    }

    /*
     * returns true if event should be cancelled and inventory closed
     */
    private boolean saveShulkerboxInventory(Player player, Inventory inventory, boolean close) {
        if (inventory.getType() != InventoryType.CHEST) {
            return false; // not a shulker inventory
        }

        if (!inventory.getTitle().equals("Shulker Box")) {
            return false; // not a shulker inventory
        }

        if (!opened.containsKey(player.getUniqueId())) {
            return false; // does not have shulkerbox open
        }

        ItemStack shulkerBox = player.getInventory().getItemInMainHand();
        if (shulkerBox == null || !isShulkerBox(shulkerBox.getType())) {
            return true; // not holding a shulker box (return true to cancel event and close inventory)
        }

        if (!opened.get(player.getUniqueId()).equals(shulkerBox)) {
            return true; // inventory does not belong to this item (return true to cancel event and close inventory)
        }

        BlockStateMeta meta = (BlockStateMeta) shulkerBox.getItemMeta();
        ShulkerBox state = (ShulkerBox) meta.getBlockState();

        state.getInventory().setContents(inventory.getContents());
        meta.setBlockState(state);

        shulkerBox.setItemMeta(meta);

        if (close) {
            opened.remove(player.getUniqueId());
        }

        return false;
    }

    private boolean isShulkerBox(Material material) {
        switch (material) {
            case WHITE_SHULKER_BOX:
            case ORANGE_SHULKER_BOX:
            case MAGENTA_SHULKER_BOX:
            case LIGHT_BLUE_SHULKER_BOX:
            case YELLOW_SHULKER_BOX:
            case LIME_SHULKER_BOX:
            case PINK_SHULKER_BOX:
            case GRAY_SHULKER_BOX:
            case SILVER_SHULKER_BOX:
            case CYAN_SHULKER_BOX:
            case PURPLE_SHULKER_BOX:
            case BLUE_SHULKER_BOX:
            case BROWN_SHULKER_BOX:
            case GREEN_SHULKER_BOX:
            case RED_SHULKER_BOX:
            case BLACK_SHULKER_BOX:
                return true;
            default:
                return false;
        }
    }
}
